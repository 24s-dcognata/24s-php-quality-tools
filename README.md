# 24S CodeQuality Package

## Embded Code Quality tools

| Tool    | Version | PHP  | Symfony |
|:--------|:--------|:-----|:--------|
| phpstan | ^1.10   | ~7.4 | ~4.4    |
| phpunit | ^9.6    | ~7.4 | ~4.4    |

## Installation

```json
{
    "repositories":[
        {
            "type": "vcs",
            "url": "git@gitlab.com:24s-dcognata/24s-php-quality-tools.git"
        }
    ],
    "require-dev": {
        "24s/php-quality-tools": "^0.1"
    }
}
```